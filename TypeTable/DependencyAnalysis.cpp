/////////////////////////////////////////////////////////////////////
// DependencyAnalysis.cpp - Test Stub for TypeTable				   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:    Code Analyzer  CSE687 Pr2, Mar-7               //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////

# include "../TypeTable/TypeTable.h"

using namespace CodeAnalysis;
int main() {
	TypeAnal1 ta1;
	ta1.doTypeAnal();

}