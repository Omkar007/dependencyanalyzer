/////////////////////////////////////////////////////////////////////
// StrongDependencyAnalyse.h - directed graph using tarzan's algorithm//
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Dependency Analyzer CSE687 Pr2, Mar-7           //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu  
/////////////////////////////////////////////////////////////////////
/*
* - Manual Information
This package creates the strong component graph using tarzan's algorithm.

Public Interface:
=================
void addEdge(int v, int w);																		// function to add an edge to graph
void setGraphSize(int a);																		// to set the graph size
void setFileGraphPath(std::unordered_map<int, std::string> setVal) {get_MapVal=setVal;}			//to get the files from key
void SCC();																						// prints strongly connected components


* - Build Process:
Required files
- StrongDependencyAnalyse.h,StrongDependencyAnalyse.cpp
Build commands
This is a static library builds with code analyzer
- devenv CodeAnalyzer.sln

* - Maintenance information
ver 1.0 : 07 Mar 2017
-	first release
*/

#include <string>
#include<iostream>
#include <list>
#include <stack>
#include <algorithm>
#include "../NOSqL/NoSqlDb.h"
#include "../NOSqL/XmlDocument/XmlWriter.h"

#include<unordered_map>
#define NIL -1
using namespace std;
using Key = std::string;
using StrData = std::string;
using Keys = std::vector<std::string>;
// A class that represents an directed graph
class Graph
{
	int V;    // No. of vertices
	list<int> *adj;    // A dynamic array of adjacency lists
	std::unordered_map<int, std::string> get_MapVal;
	// A Recursive DFS based function used by SCC()
	void SCCUtil(int u, int disc[], int low[],
		stack<int> *st, bool stackMember[]);
		NoSqlDb<std::string> dbStrong;
		XmlParserDB<std::string> xml;

public:
	Graph();   // Constructor
	void addEdge(int v, int w);   // function to add an edge to graph
	void setGraphSize(int a);
	void setFileGraphPath(std::unordered_map<int, std::string> setVal) { get_MapVal = setVal; }
	void displayStrongComponentDB(std::string fileName);
	void SCC();    // prints strongly connected components
};

Graph::Graph()
{
}

void Graph::addEdge(int v, int w)
{
	adj[v].push_back(w);
}

void Graph::setGraphSize(int a)
{
	this->V = a;
	adj = new list<int>[a];
}

// A recursive function that finds and prints strongly connected
// components using DFS traversal
void Graph::SCCUtil(int u, int disc[], int low[], stack<int> *st,
	bool stackMember[])
{
	static int time = 0;
	// Initialize discovery time and low value
	disc[u] = low[u] = ++time;
	st->push(u);
	stackMember[u] = true;

	// Go through all vertices adjacent to this
	list <string> strongComponents;
	list<int>::iterator i;
	for (i = adj[u].begin(); i != adj[u].end(); ++i)
	{
		int v = *i;  // v is current adjacent of 'u'
					 // If v is not visited yet, then recur for it
		if (disc[v] == -1)
		{
			SCCUtil(v, disc, low, st, stackMember);
			low[u] = min(low[u], low[v]);
		}
		else if (stackMember[v] == true)
			low[u] = min(low[u], disc[v]);
	}
	int w = 0;  // To store stack extracted vertices
	if (low[u] == disc[u])
	{
		cout << "\n" << "Strong Components:"<<endl;
		while (st->top() != u)
		{
			w = (int)st->top();
			std::cout << get_MapVal[w] << " \t";
			stackMember[w] = false;
			st->pop();
		}
		w = (int)st->top();
		std::cout << get_MapVal[w] <<"\n";
		stackMember[w] = false;
		st->pop();
	}
}

//Displaying the Strong Components stored in db
inline void Graph::displayStrongComponentDB(std::string fileName)
{
	std::string name;
	DBElement<std::string> elem;
	std::cout << "\n\n Requirement 6 - find strong components in the dependency graph defined by the relationships evaluated in the previous requirement. \n";
	std::cout << "-------------------- Displaying Strong Componenents in NoSql Database -------------------------- \n";
	for (auto it : get_MapVal) {	
		name = "strong components";
		elem.setName(name);
		elem.getchildKeys().push_back(it.second+"\n");
	}
	dbStrong.save(elem.getName(), elem);
	Keys keys = dbStrong.keys();
	for (Key itm : keys) {
		std::cout << " " << itm;
		std::cout << " " << dbStrong.value(itm).show();
	}
	std::cout << "\n Requirement - 7,8 Writing the analysis results of Strong Component, in XML format, to a specified file at Path --> ../Test/" + fileName + "\n";
	std::string xmlFiles = xml.PersistToDB(dbStrong);
	xml.WriteXml(xmlFiles, fileName);
	std::cout << xml.ReadXmlFile(fileName);
}

// The function to do DFS traversal. It uses SCCUtil()
void Graph::SCC()
{
	int *disc = new int[V];
	int *low = new int[V];
	bool *stackMember = new bool[V];
	stack<int> *st = new stack<int>();

	// Initialize disc and low, and stackMember arrays
	for (int i = 0; i < V; i++)
	{
		disc[i] = NIL;
		low[i] = NIL;
		stackMember[i] = false;
	}

	// Call the recursive helper function to find strongly
	// connected components in DFS tree with vertex 'i'
	for (int i = 0; i < V; i++)
		if (disc[i] == NIL)
			SCCUtil(i, disc, low, st, stackMember);
}