#pragma once
/////////////////////////////////////////////////////////////////////
// DepAnal.h -		To generate and Analyze type table		       //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Dependency Analyzer CSE687 Pr2, Mar-7           //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////
/*
* - Manual Information
This package provides the

Public Interface:
=================
void DFS(ASTNode* pNode);				//Recursively traversing tree by calling DFS
bool doDisplay1(ASTNode* pNode)			//Displaying ASTNode tree 
doTypeAnal()							//function to do type analysis

* - Build Process:
Required files
- DepAnal.h, ActionsAndRules.h,DepAnal.cpp
Build commands (either one)
- devenv CodeAnalyzerEx.sln

* - Maintenance information
ver 1.0 : 07 Feb 2017
-	first release
*/


#include "../Parser/ActionsAndRules.h"
#include <iostream>
#include <functional>

#pragma warning (disable : 4101)  // disable warning re unused variable x, below

namespace CodeAnalysis
{
	class TypeAnal
	{
	public:
		using SPtr = std::shared_ptr<ASTNode*>;

		TypeAnal();
		void doTypeAnal();
	private:
		void DFS(ASTNode* pNode);
		AbstrSynTree& ASTref_;
		ScopeStack<ASTNode*> scopeStack_;
		Scanner::Toker& toker_;
	};

	inline TypeAnal::TypeAnal() :
		ASTref_(Repository::getInstance()->AST()),
		scopeStack_(Repository::getInstance()->scopeStack()),
		toker_(*(Repository::getInstance()->Toker()))
	{
		std::function<void()> test = [] { int x; };  // This is here to test detection of lambdas.
	}                                              // It doesn't do anything useful for dep anal.

	inline bool doDisplay1(ASTNode* pNode)
	{
		static std::string toDisplay[] = {
			"function", "lambda", "class", "struct", "enum", "alias", "typedef"
		};
		for (std::string type : toDisplay)
		{
			if (pNode->type_ == type)
				return true;
		}
		return false;
	}
	inline void TypeAnal::DFS(ASTNode* pNode)
	{
		static std::string path = "";
		if (pNode->path_ != path)
		{
			std::cout << "\n    -- " << pNode->path_ << "\\" << pNode->package_;
			path = pNode->path_;
		}
		if (doDisplay1(pNode))
		{
			std::cout << "\n  " << pNode->name_;
			std::cout << ", " << pNode->type_;
		}
		for (auto pChild : pNode->children_)
			DFS(pChild);
	}

	inline void TypeAnal::doTypeAnal()
	{
		std::cout << "\n  starting type analysis:\n";
		std::cout << "\n  scanning AST and displaying important things:";
		std::cout << "\n -----------------------------------------------";
		ASTNode* pRoot = ASTref_.root();
		DFS(pRoot);
		std::cout << "\n\n  the rest is for you to complete\n";
	}
}