/////////////////////////////////////////////////////////////////////
// DependencyAnalysis.h -		Test Stub				           //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Dependency Analyzer CSE687 Pr2, Mar-7           //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////
/*
*/

#include "../DependencyAnalysis/DependancyAnalysis.h"

int main() {
	std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> my_typeTable;
	DependencyAnalysis dep;
	dep.getFilesFromDir(my_typeTable);
}