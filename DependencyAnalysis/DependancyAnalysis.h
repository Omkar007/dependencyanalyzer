#pragma once
/////////////////////////////////////////////////////////////////////
// DependencyAnalysis.h - Analalyzes dependencies and creates dependency table   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Dependency Analyzer CSE687 Pr2, Mar-7           //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //	
/////////////////////////////////////////////////////////////////////
/*
* - Manual Information
This package analyzes the dependencies among the files and creates the dependency table

Public Interface:
=================
std::unordered_map<std::string, std::set<std::string>> DependencyAnalyzer(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable, ASTNode* pNode);			//Used to store the dependency table
void displayDepencyTable(std::unordered_map<std::string, std::set<std::string>> & dept_Table);																										//Used to display the dependency table
std::unordered_map<std::string, std::set<std::string>>& getDependencyFilesMap() { return dependencyTable; }																							//getter method to return dependency table
void DependencyAnalysis::displayDB();																																								//Used to display Nosql db contents
void getFilesFromDir(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable);																				// To get the files from directories recursively
void StrongComponentRequirement();																																									// to create strong component graph

* - Build Process:
Required files
- DependencyAnalysis.h,DependencyAnalysis.cpp
Build commands
This is a static library builds with code analyzer
- devenv CodeAnalyzer.sln

* - Maintenance information
ver 1.0 : 07 Mar 2017
-	first release
*/

#include "../Tokenizer/Tokenizer.h"
#include "../Utilities/Utilities.h"
#include "../TypeTable/TypeTable.h"
#include "../Parser/ActionsAndRules.h"
#include "../NOSqL/NoSqlDb.h"
#include "../FileSystem/FileSystem.h"
#include "../NOSqL/XmlDocument/XmlElement/XmlElement.h"
#include "../NOSqL/XmlDocument/XmlWriter.h"
#include "../StrongDependencyAnalyzer/StrongDependencyAnalyse.h"
#include <set>
#include <iostream>
#include <iomanip>

using namespace Scanner;
using namespace CodeAnalysis;
using namespace XmlProcessing;

using Key = std::string;
using StrData = std::string;
using Keys = std::vector<std::string>;
class DependencyAnalysis {

public:
	DependencyAnalysis();
	std::unordered_map<std::string, std::set<std::string>> DependencyAnalyzer(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable, ASTNode* pNode);
	void displayDepencyTable(std::unordered_map<std::string, std::set<std::string>> & dept_Table);
	std::unordered_map<std::string, std::set<std::string>>& getDependencyFilesMap() { return dependencyTable; }
	void DependencyAnalysis::displayDB(std::string fileName);
	void getFilesFromDir(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable);
	void StrongComponentRequirement(std::string fileName);
	std::unordered_map<std::string, int> graph_map;
private:
	std::unordered_map<std::string, std::set<std::string>> dependencyTable;
	NoSqlDb<std::string>db;
	AbstrSynTree& ASTref_;
	FileSystem::Directory directories;
	FileSystem::Path extensions;
	XmlParserDB<std::string> xml;
};

inline DependencyAnalysis::DependencyAnalysis() :ASTref_(Repository::getInstance()->AST()) {}

//Function to insert the dependency analysis in the dependency analysis table
std::unordered_map<std::string, std::set<std::string>> DependencyAnalysis::DependencyAnalyzer(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable, ASTNode* pNode)
{
	std::string files;
	static std::string path = "";
	try {
		files = pNode->path_;
		std::ifstream in(files);
		if ((!in.good()) && pNode->path_ == path) {}
		else
		{	Toker toker;
			toker.returnComments();
			toker.attach(&in);
			do
			{
				std::string tokens = toker.getTok();
				for (auto typetable : my_typeTable)
				{
					for (int i = 0; i < typetable.second.size(); i++)
					{
						if (typetable.first == tokens)
						{
							if (files!=typetable.second[i].second)
							{
								dependencyTable[files];
								dependencyTable[files].insert(typetable.second[i].second);
							}
							else{
								dependencyTable[files];
								}
						}}
				}
			} while (in.good());
		}
		for (auto pChild : pNode->children_)
			DependencyAnalyzer(my_typeTable, pChild);
	}
	catch (std::logic_error& ex)
	{std::cout << "\n  " << ex.what();
	}
	return dependencyTable;
}

//Function to display the dependency analysis table and persist it in database
inline void DependencyAnalysis::displayDB(std::string fileName) {
	displayDepencyTable(dependencyTable);
	std::set<std::string>::iterator itr;
	for (auto it : dependencyTable) {
		std::string name;
		DBElement<std::string> elem;
		name = it.first;
		elem.setName(name);
		for (itr = it.second.begin(); itr != it.second.end(); ++itr)
		{
			if (!(*itr == it.first)) { elem.getchildKeys().push_back(*itr); }		}
		db.save(elem.getName(), elem);
	}
	Keys keys = db.keys();
	std::cout << "\n\n=================== - NOSQLDB-BONUS==========================\n\n";
	for (Key it : keys) {
		std::cout << " " << it;
		std::cout << " " << db.value(it).show();
	}
	std::cout << "\n Requirement - 7,8 Writing the analysis results, in XML format, to a specified file at Path --> ../Test/"+ fileName+"\n";
	std::string xmlFiles = xml.PersistToDB(db);
	std::cout << xmlFiles;
	xml.WriteXml(xmlFiles, fileName);
	std::cout << xml.ReadXmlFile(fileName);
}

//Function to display the dependency analysis table
inline void DependencyAnalysis::displayDepencyTable(std::unordered_map<std::string, std::set<std::string>> & my_depTable)
{
	std::cout << "\n\n Requirement-5 Shall provide a DependencyAnalysis package that identifies all of the dependencies between files in a specified file collection.\n\n";
	int length1 = 50, length2 = 50;
	std::set<std::string>::iterator it_in;
	for (auto it : dependencyTable) {
		std::cout<<"\n\n" << std::setw(length1) << std::left << it.first<<endl;
		std::cout << "Dependencies are :";
		std::cout << " ";
		for (it_in = it.second.begin(); it_in != it.second.end(); ++it_in) {
			std::cout << *it_in<<",";
		}
	}
}

//Function to generate the package dependency information using the ASTNode
inline void DependencyAnalysis::getFilesFromDir(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& dept_Table)
{
	ASTNode* pRoot = ASTref_.root();
	DependencyAnalyzer(dept_Table, pRoot);
}

//Function to generate the Strong Component of the graph
inline void DependencyAnalysis::StrongComponentRequirement(std::string fileName)
{
	std::unordered_map<int, std::string> setVal;
	Graph graph;
	graph.setGraphSize((int)db.keys().size());
	int k = 0;

	for (std::string key : db.keys())
	{
		graph_map[key] = k;
		setVal[k] = key;
		k++;
	}
	for (std::string key : db.keys())
	{
		DBElement<StrData> dependencies = db.value(key);
		std::vector<std::string> myVec = dependencies.getchildKeys();
		for (std::string child : myVec)
		{
			if (!(key == child))
			{
				graph.addEdge(graph_map[key], graph_map[child]);
			}
		}
	}
	graph.setFileGraphPath(setVal);
	graph.SCC();
	graph.displayStrongComponentDB(fileName);
}
